# Buscaminas
import random
import os
import msvcrt

RED = '\033[31m'
YELLOW = '\033[33m'
GREEN = '\033[32m'
WHITE = '\033[39m'
marca = (RED + "X" + WHITE)
bandera = (YELLOW + "¶" + WHITE)
bomba = (GREEN + "☼" + WHITE)
celda = "■"


def filycol(opcion):
    if opcion == 1:
        filas = 8
        columnas = 10
        minas = 8
        return filas, columnas, minas
    elif opcion == 2:
        filas = 14
        columnas = 18
        minas = 20
        return filas, columnas, minas
    elif opcion == 3:
        os.system("cls")
        filas = int(input("Ingrese la cantidad de filas: "))
        columnas = int(input("Ingrese la cantidad de columnas: "))
        minas = int(input("Ingrese la cantidad de minas: "))
        return filas, columnas, minas


opcion = int(input("Opcion 1: Chico (10x8,10)\nOpcion 2: Grande (18x14,40)\nOpcion 3: Personalizado\nOpcion a elegir: "))
filas, columnas, minas = filycol(opcion)


def tablero(fil, col, val):
    """
    Crea una lista de listas con la cantidad de filas, columnas y valor correspondientes
    """
    tablero_llena = []
    for y in range(fil):
        tablero_llena.append([])
        for x in range(col):
            tablero_llena[y].append(val)
    return tablero_llena


def muestra_tablero(tab):
    """
    Acomoda los valores de cada tablero para que se vean sin comas
    """
    borde = "* " * (columnas + 2)
    print("\tBUSCAMINAS")
    print(borde)
    for fila in tab:
        print("*", end=" ")
        for elemento in fila:
            print(elemento, end=" ")
        print("*")
    print(borde)


def coloca_minas(tablero_molde, minas_cantidad, fil, col):
    """
    Coloca las minas en lugares aleatorios y los guarda en la lista minas_ocultas
    """
    minas_escondidas = []
    num = 0
    while num < minas_cantidad:
        y = random.randint(0, fil - 1)
        x = random.randint(0, col - 1)
        if tablero_molde[y][x] != 9:
            tablero_molde[y][x] = 9
            num += 1
            minas_escondidas.append((y, x))
    return tablero_molde, minas_escondidas


def salvavidas_primero_paso(tablero_molde, minas_cantidad, posy, posx, fil, col):
    """
    Se encarga de evitar que la primer muestra de celda no sea bomba y el usuario pierda
    """
    minas_escondidas = []
    num = 0
    while num < minas_cantidad:
        y = random.randint(0, fil - 1)
        x = random.randint(0, col - 1)
        if tablero_molde[y][x] != 9 and (y != posy) and (x != posx):
            tablero_molde[y][x] = 9
            num += 1
            minas_escondidas.append((y, x))
    return tablero_molde, minas_escondidas


def coloca_numeros(tablero, fil, columnas):
    """
    Se encarga de colocar los numeros alrededor de cada bomba
    """
    for y in range(fil):
        for x in range(columnas):
            if tablero[y][x] == 9:
                for i in [-1, 0, 1]:
                    for j in [-1, 0, 1]:
                        if 0 <= y + i <= fil - 1 and 0 <= x + j <= columnas - 1:
                            if tablero[y + i][x + j] != 9:
                                tablero[y + i][x + j] += 1
    return tablero


def relleno(oculto, visible, y, x, fil, col, val):
    """
    Se encarga de recorrer las casillas vecinas de la posicion que marcamos y si no contienen bombas cerca,
    los revela hasta encontrar aquellas celdas que presenten bombas cerca
    """
    ceros = [(y, x)]
    while len(ceros) > 0:
        y, x = ceros.pop()
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if 0 <= y + i <= fil - 1 and 0 <= x + j <= col - 1:
                    if visible[y + i][x + j] == val and oculto[y + i][x + j] == 0:
                        visible[y + i][x + j] = 0
                        if (y + i, x + j) not in ceros:
                            ceros.append((y + i, x + j))
                    else:
                        visible[y + i][x + j] = oculto[y + i][x + j]
    return visible


def tablero_completo(tablero, fil, col, val):
    """
    Comprueba que el tablero se encuentre sin ninguna celda por ejecutar
    """
    for y in range(fil):
        for x in range(col):
            if tablero[y][x] == val:
                return False
    return True


def reemplaza_ceros(tablero):
    """
    Reemplaza los ceros del tablero por espacios vacios
    """
    for i in range(filas):
        for j in range(columnas):
            if tablero[i][j] == 0:
                tablero[i][j] = " "
    return tablero


def menu():
    """
    Muestra las opciones del juego y pide que ingrese la opcion deseada
    """
    print("A -> Izquierda")
    print("S -> Abajo")
    print("D -> Derecha")
    print("W -> Arriba")
    print("B -> Poner bandera")
    print("V -> Sacar bandera")
    print("SPACE -> Marcar celda")
    opcion = msvcrt.getwch()
    return opcion


oculto = tablero(filas, columnas, 0)  # Crea el tablero oculto llenos de ceros.
visible = tablero(filas, columnas, celda)  # Crea el tablero visible lleno de guiones.

oculto, minas_ocultas = coloca_minas(oculto, minas, filas, columnas)    # Coloca las minas dentro del tablero oculto.
cantidad_banderas = len(minas_ocultas)    # Variable para el contador de banderas marcadas.

oculto = coloca_numeros(oculto, filas, columnas)    # Indica en cada celda cuantas bombas tiene cerca.

y, x = filas // 2, columnas // 2  # Variables para desplazar la "X" en eje Y y eje X.

temporal = visible[y][x]
visible[y][x] = marca  # Posicion de la X con la cual jugaremos.

os.system("cls")

muestra_tablero(visible)  # Muestra el primer tablero, el cual seria el visible.
banderas = []

running = True  # Variable para el inicio del juego.
win = False
primer_paso = 0
while running:
    print(f"\nCantidad de banderas: {cantidad_banderas}")
    pos = menu()
    if pos == "w" or pos == "W":  # Movimiento para Arriba.
        if y == 0:
            y = 0
        else:
            visible[y][x] = temporal
            y -= 1
            temporal = visible[y][x]
            visible[y][x] = marca

    elif pos == "s" or pos == "S":  # Movimiento para Abajo.
        if y == filas - 1:
            y = filas - 1
        else:
            visible[y][x] = temporal
            y += 1
            temporal = visible[y][x]
            visible[y][x] = marca

    elif pos == "a" or pos == "A":  # Movimiento para la Izquierda.
        if x == 0:
            x = 0
        else:
            visible[y][x] = temporal
            x -= 1
            temporal = visible[y][x]
            visible[y][x] = marca

    elif pos == "d" or pos == "D":  # Movimiento para la Derecha.
        if x == columnas - 1:
            x = columnas - 1
        else:
            visible[y][x] = temporal
            x += 1
            temporal = visible[y][x]
            visible[y][x] = marca

    elif pos == "b" or pos == "B":  # Marca una bandera.
        if temporal == celda:
            cantidad_banderas -= 1
            visible[y][x] = bandera
            temporal = visible[y][x]
            if (y, x) not in banderas:
                banderas.append((y, x))

    elif pos == "v" or pos == "V":  # Desmarca una bandera.
        if temporal == bandera:
            cantidad_banderas += 1
            visible[y][x] = celda
            temporal = visible[y][x]
            if (y, x) in banderas:
                banderas.remove((y, x))

    elif pos == " ":  # Se selecciona la posicion y se la revela.
        if temporal == bandera:   # Si se marca una celda que contenia una bandera, se remueve de la lista de banderas.
            cantidad_banderas += 1
            if (y, x) in banderas:
                banderas.remove((y, x))

        if primer_paso == 0 and oculto[y][x] == 9:
            oculto = tablero(filas, columnas, 0)  # Crea el tablero oculto llenos de ceros.
            oculto, minas_ocultas = salvavidas_primero_paso(oculto, minas, y, x, filas, columnas)  # Coloca las minas dentro del tablero oculto.
            oculto = coloca_numeros(oculto, filas, columnas)  # Indica en cada celda cuantas bombas tiene cerca.
            primer_paso += 1

        if oculto[y][x] == 9:
            visible[y][x] = bomba
            for i, j in minas_ocultas:
                visible[i][j] = bomba
            running = False

        elif oculto[y][x] != 0:
            visible[y][x] = oculto[y][x]
            temporal = visible[y][x]

        elif oculto[y][x] == 0:
            visible[y][x] = 0
            visible = relleno(oculto, visible, y, x, filas, columnas, celda)
            visible = reemplaza_ceros(visible)
            temporal = visible[y][x]
        primer_paso += 1

    os.system("cls")
    muestra_tablero(visible)

    if tablero_completo(visible, filas, columnas, celda) and sorted(minas_ocultas) == sorted(banderas) and temporal != celda:
        win = True
        running = False
if not win:
    print("\n-------- PERDISTE --------")
    os.system("pause")
else:
    print("\n-------- GANASTE --------")
    os.system("pause")